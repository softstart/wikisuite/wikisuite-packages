#!/bin/bash

BASE_DIR=$(cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd)

{
echo "Executing WikiSuite post server change scripts..."
echo

if [ ! -d "${BASE_DIR}/post.d" ] ; then
        echo "Post server change script dir not found ${BASE_DIR}/post.d"
else
        for CMD in "${BASE_DIR}/post.d"/* ; do
                if [ -x $CMD ] ; then
                        echo "== Executing ${CMD} .."
                        $CMD
                        echo "== .. done (Executing)"
                fi
        done
fi

echo ".. done (WikiSuite scripts)"
} | sed 's/$/<br>/'
