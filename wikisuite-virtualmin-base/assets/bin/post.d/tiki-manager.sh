#!/bin/bash

if [ "${VIRTUALSERVER_VIRTUALMIN_TIKIMANAGER}" != "1" ] ;  then
	echo "Tiki Manager not enabled for domain, skipping"
	exit 0
fi

VIRTUALMIN=$(command -v virtualmin)
if [ -z "${VIRTUALMIN}" ] ; then
	echo "Could not find the Virtualmin command, exiting"
	exit 1
fi

$VIRTUALMIN tiki-update-php --domain ${VIRTUALSERVER_DOM}
